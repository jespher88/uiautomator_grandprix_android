package br.com.rasini.teste.app

import android.support.test.InstrumentationRegistry
import android.support.test.filters.SdkSuppress
import android.support.test.runner.AndroidJUnit4
import android.support.test.uiautomator.UiDevice
import android.support.test.uiautomator.UiSelector
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@SdkSuppress(minSdkVersion = 18)


class Abrir {

    private val bottonSelfiePaddock = "SELFIE PADDOCK"
    private val bottonMatchTheCar = "MATCH THE CAR"


    lateinit var mGrandPrix: UiDevice

    @Before
    fun before() {
        mGrandPrix = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        Assert.assertThat(mGrandPrix, CoreMatchers.notNullValue())

        //Vai a Home do device
        mGrandPrix.pressHome()


    }

    @Test
    fun abrirApp() {

        //Clica no launcher do app
        clickByText("Grand Prix")



        //Validação do teste: Valida a escrita do botão SelfiePaddock
        val validarTexto = mGrandPrix.findObject(UiSelector()
                .resourceId("com.dxc.digitalservices.grandprix:id/btn_selfieStudio"))
        Assert.assertThat(validarTexto.text, CoreMatchers.`is`(CoreMatchers.equalTo(bottonSelfiePaddock)))


        //Clica no botão SELFIE PADDOCK
        clickByResID("com.dxc.digitalservices.grandprix:id/btn_selfieStudio")




        //Validação do teste: Valida a escrita do botão MatchTheCar
        val validarTexto2 = mGrandPrix.findObject(UiSelector()
                .resourceId("com.dxc.digitalservices.grandprix:id/cameraBtn"))
        Assert.assertThat(validarTexto2.text, CoreMatchers.`is`(CoreMatchers.equalTo(bottonMatchTheCar)))


        //Clica no botão MATCH THE CAR
        clickByResID("com.dxc.digitalservices.grandprix:id/cameraBtn")




        //Validação do teste: Valida a o botão de flip
        val iconeFlip = mGrandPrix.findObject(UiSelector()
                .className("android.widget.ImageButton").index(2))
        Assert.assertThat(iconeFlip.className, CoreMatchers.`is`(CoreMatchers.equalTo("android.widget.ImageButton")))


        //Clica no botão de flip
        clickByResID("com.dxc.digitalservices.grandprix:id/flip")


        //Validação do teste: Valida o botão de tirar foto
        val iconeTakePicture = mGrandPrix.findObject(UiSelector()
                .className("android.widget.ImageButton").index(3))
        Assert.assertThat(iconeTakePicture.className, CoreMatchers.`is`(CoreMatchers.equalTo("android.widget.ImageButton")))



        //Clica no botão de tirar foto
        clickByResID("com.dxc.digitalservices.grandprix:id/take_picture")

    }


    private fun clickByText(text: String) {

        mGrandPrix.findObject(UiSelector().text(text)).click()

    }

    private fun clickByResID(resID: String) {

        mGrandPrix.findObject(UiSelector().resourceId(resID)).click()

    }
}