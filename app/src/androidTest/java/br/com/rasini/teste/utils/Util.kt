package br.com.rasini.teste.utils

import android.support.test.uiautomator.UiDevice
import android.support.test.uiautomator.UiObjectNotFoundException
import android.support.test.uiautomator.UiScrollable
import android.support.test.uiautomator.UiSelector

class Util {
    class Util {
        companion object {

            fun beforeAvisoDeViagem(uiDevice: UiDevice) {
                //Antes de cada teste será realizado o Login no app.

                uiDevice.pressHome()
                uiDevice.findObject(UiSelector().text("Cartões")).click()

                uiDevice.findObject(UiSelector().resourceId("br.com.portoseguro.cartoes:id/etSenha"))
                        .setText("@@Porto123").toString()

                uiDevice.findObject(UiSelector().resourceId("br.com.portoseguro.cartoes:id/btnEntrar")).click()

                sleepTime()
            }

            fun matarApp(uiDevice: UiDevice) {
                uiDevice.pressRecentApps()
                uiDevice.findObject(UiSelector().packageName("com.android.systemui")
                        .checkable(false)
                        .checked(false)
                        .enabled(true)
                        .clickable(true)
                        .focusable(false)
                        .focused(false)
                        .scrollable(false)
                        .longClickable(false)
                        .selected(false))
                        .click()
            }

            fun loginPrimario(uiDevice: UiDevice) {

                uiDevice.findObject(UiSelector().text("PERMITIR")).click()
                uiDevice.findObject(UiSelector().text("PERMITIR")).click()
                uiDevice.findObject(UiSelector().text("Já tenho conta")).click()
                uiDevice.findObject(UiSelector().text("Digite seu CPF")).click()


                uiDevice.findObject(UiSelector().className("android.widget.EditText"))
                        .setText("32036703801").toString()
                uiDevice.findObject(UiSelector().text("Digite sua senha do Portal do Cliente")).click()


                uiDevice.findObject(UiSelector().resourceId("br.com.portoseguro.cartoes:id/etSenha"))
                        .setText("@@Porto123").toString()
                uiDevice.findObject(UiSelector().resourceId("br.com.portoseguro.cartoes:id/btnEntrar")).click()

                scrollToEnd()

                uiDevice.findObject(UiSelector().text("Aceitar e continuar")).click()




            }

            fun clickInContinue(uiDevice: UiDevice) {

                uiDevice.findObject(UiSelector().text("Continuar")).click()
            }

            fun sleepTime() {
                try {
                    Thread.sleep(3000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }

            fun scrollFlingForward() {
                val prodPorSeg = UiScrollable(UiSelector().scrollable(false))
                try {
                    prodPorSeg.flingForward()
                } catch (e: UiObjectNotFoundException) {
                    e.printStackTrace()

                }
            }

            fun scrollToEnd() {
                val appViews = UiScrollable(UiSelector().scrollable(true))
                try {
                    appViews.scrollToEnd(100, 4)
                } catch (e: UiObjectNotFoundException) {
                    e.printStackTrace()
                }
            }

            fun scrollToBeginning() {

                val appViews = UiScrollable(UiSelector().scrollable(true))
                try {
                    appViews.scrollToBeginning(100, 4)
                } catch (e: UiObjectNotFoundException) {
                    e.printStackTrace()
                }

            }

            fun scrollForward() {
                val appViews = UiScrollable(UiSelector().scrollable(true))
                try {
                    appViews.scrollForward()
                } catch (e: UiObjectNotFoundException) {
                    e.printStackTrace()
                }

            }

            fun sleepTimeAndGoHome(uiDevice: UiDevice) {
                try {
                    Thread.sleep(2000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
                //Ir para Home do device
                uiDevice.pressHome()

            }


        }

    }
}